#x(t) = a * t * cos(t), y(t) = a * t * sin(t)
      
  
from random import randint
import sqlite3
import os

galSize = (800,800)
galPadding = 5

db = "database.sql"
conn = sqlite3.connect(db)
c = conn.cursor()
   
c.execute('''SELECT name FROM sqlite_master WHERE type='table' AND name="systems"''')
c_1 = c.fetchall()
if not len(c_1) > 0:
    c.execute('''create table systems(sid integer, name TEXT, jumppoints integer, x integer, y integer)''')
    c.execute('''create table locations(lid integer, sid integer, type text, name text, development integer, control integer, food integer,
   light integer, heavy integer, luxury integer, luxury_o integer, organic_industrial integer)''')
    c.execute('''create table goods(gid INTEGER PRIMARY KEY AUTOINCREMENT, lid integer, type text, importexport text, qty integer)''')
    c.execute('''create table jumppoints(jid INTEGER PRIMARY KEY AUTOINCREMENT, origin integer, destination integer)''')
   
types = {'Spacestation': {
    'development':0,
    'control':3,
    'industry': {
        'food_industrial':1,
        'light_industry':-1,
        'heavy_industry':-1,
        'luxury_industrial':-1,
        'luxury_organics':-1,
        'organic_industrial':-1,
        },
    },
         'Asteroid Colony': {
    'development':1,
    'control':3,
    'industry': {
        'food_industrial':0,
        'light_industry':2,
        'heavy_industry':1,
        'luxury_industrial':-1,
        'luxury_organics':-2,
        'organic_industrial':-1,
        },
    },
        "Rockball": {
    'development':0,
    'control':4,
    'industry': {
        'food_industrial':-1,
        'light_industry':1,
        'heavy_industry':1,
        'luxury_industrial':0,
        'luxury_organics':0,
        'organic_industrial':0,
        },
    },
        "Desert": {
    'development':0,
    'control':3,
    'industry': {
        'food_industrial':1,
        'light_industry':1,
        'heavy_industry':0,
        'luxury_industrial':0,
        'luxury_organics':-1,
        'organic_industrial':1,
        },
    },
        "Hostile": {
    'development':0,
    'control':3,
    'industry': {
        'food_industrial':1,
        'light_industry':0,
        'heavy_industry':0,
        'luxury_industrial':0,
        'luxury_organics':2,
        'organic_industrial':2,
        },
    },
        "Icy": {
    'development':1,
    'control':1,
    'industry': {
        'food_industrial':-1,
        'light_industry':1,
        'heavy_industry':-1,
        'luxury_industrial':-2,
        'luxury_organics':1,
        'organic_industrial':1,
        },
    },
        "Terran": {
    'development':2,
    'control':4,
    'industry': {
        'food':2,
        'light_industry':-2,
        'heavy_industry':1,
        'luxury_industrial':0,
        'luxury_organics':0,
        'organic_industrial':0,
        },
    },
         }
   
goods = {
    'food': ('wheat', 'corn', 'barley', 'beef', 'pork',),
    'food_industrial': ('vat-meat', 'soy', 'hydrogarden'),
    'light_industry': ('iron', 'steel', 'tungsten', 'platinum',
                       'gold', 'aluminium', 'building_materials', 'prefabs',),
    'heavy_industry':('computers','generators','light_industrial_machinery',
                      'heavy_industrial_machinery','adv_fuel',
                      'starship_parts','station_parts',
                      'synthetic_medicene','synthetic_fabric',),
    'luxury_industrial':('movies','music','synthetic_drugs','gemstones',),
    'luxury_organics':('spices', 'herbal_medicene','flowers','luxury_fabric','confectionary'),
    'organic_industrial':('exotic_wood','timber','organic_medicene',
                          'fossil_fuel','biofuel','wool','cloth','leather',),
    }
def dice(number, size):
    result = 0
    for die in range(number):
        result += randint(1,size)
    return result
   
class Colony:
    def __init__(self, coltype, name,sid,lid):
        self.name = name
        self.type = coltype
        self.development = randint(1,6)+types[coltype]['development']
        if self.development <= 0:
            self.development = 1
        self.control = randint(1,6)+types[coltype]['control']
        if self.control <= 0:
            self.control = 1
        self.industry = types[coltype]['industry']
        self.exports = {}
        self.imports = {}
   
        for i in self.industry:
            if not 'food' in i:
                self.industry[i] += randint(0,4)
            else:
                self.industry[i] += randint(0,4)-self.development
        for i in self.industry:
            if self.industry[i] < 0:
                x = self.industry[i]
                if 'food' in i:
                    y = goods['food']+goods['food_industrial']
                else:
                    y = goods[i]
                while x < 0:
                    tempgood = y[randint(0,len(y)-1)]
                    if tempgood in self.imports:
                        self.imports[tempgood] += 1
                    else:
                        self.imports[tempgood] = 1
                    x += 1
            elif self.industry[i] > 0:
                x = self.industry[i]
                y = goods[i]
                while x > 0:
                    tempgood = y[randint(0,len(y)-1)]
                    if tempgood in self.exports:
                        self.exports[tempgood] += 1
                    else:
                        self.exports[tempgood] = 1
                    x -= 1
        for good in self.exports:
            tracker = (lid, good, "e", self.exports[good])
            c.execute('''insert into goods(lid,type,importexport,qty) values(?,?,?,?)''',tracker)
        for good in self.imports:
            tracker = (lid, good, 'i', self.imports[good])
            c.execute('''insert into goods(lid,type,importexport,qty) values(?,?,?,?)''',tracker)
        
   
        if 'food_industrial' in self.industry:
            values = (lid,sid, self.type, self.name,self.development, self.control, self.industry['food_industrial'],self.industry['light_industry']
                      ,self.industry['heavy_industry'],self.industry['luxury_industrial'],self.industry['luxury_organics'],self.industry['organic_industrial'],)
        else:
            values = (lid,sid, self.type, self.name,self.development, self.control, self.industry['food'],self.industry['light_industry']
                      ,self.industry['heavy_industry'],self.industry['luxury_industrial'],self.industry['luxury_organics'],self.industry['organic_industrial'],)
        c.execute('''insert into locations(lid,sid,type,name,development,control,food,light,heavy,luxury,luxury_o,organic_industrial) values(?,?,?,?,?,?,?,?,?,?,?,?)''',values)
        conn.commit()





    def report(self):
        print("This is a report for a",self.type)
        print("Development is",self.development,"and Control is",self.control)
        tempstr = ""
        for i in self.exports:
            tempstr+=str(self.exports[i])+" "+i+", "
        print("It Exports",tempstr)
        tempstr = ""
        for i in self.imports:
            tempstr+=str(self.imports[i])+" "+i+", "
        print("And Imports",tempstr)
   
   
class Galaxy:
    def __init__(self):
        self.sndCnt = -1
        self.locations = {}
        names = ["Alpha", "Beta", "Gamma", "Delta", "Centauri", "Norn", "Minbar",
                 "Procyon", "Regulus", "Orion", "Fieras", "Eleria", "Manticore", "Haven", "New Potsdam",
                 "Babylon", "Rome", "Indus", "Zhou", "Wei", "Shang", "Xia", "California", "Montana", "Texas", "York", "London",
                 "Carthage", "Antioch", "Athens",]
        israndom = 1
        sx = 0
        sy = 0
        until = 0
        while until == 0:
            try_until = input("Number of systems to generate: ")
            if try_until.isnumeric():
                try_until = int(try_until)
                print("It's numeric")
                if try_until >= 1:
                    until = try_until
                    print("It's greater than 1")
                else:
                    print("Put a number larger than 0")
            else:
                print("Put in a number larger than 0")
   
        self.count = 0
        for i in range(until):
            name = self.nameGen()
            jumppoints = [1,1,1,1,1,1,2,2,2][randint(0,8)]
            if (israndom > 0):
                sx = randint(galPadding,galSize[0]-galPadding)
                sy = randint(galPadding,galSize[1]-galPadding)
            c.execute('''insert into systems(sid,name,jumppoints, x, y) values(?,?,?,?,?)''',(i,name,jumppoints, sx, sy))
            self.locations[len(self.locations)] = System(name,i, self,jumppoints)
        conn.commit()
        
         
        for i in self.locations:
            self.locations[i].jp_list = []
            for jp in range(self.locations[i].jp):
                jp_a = "Neg"
                while jp_a == "Neg":
                    jp_a = self.get_jp(i)
                self.locations[i].jp_list.append(jp_a)
                sqltupple = (i, jp_a)
                c.execute('''insert into jumppoints(origin, destination) values(?,?)''',sqltupple)
        conn.commit()

    def get_jp(self,i):
        syslist = list(self.locations.keys())
        jp_a = syslist[randint(0,len(syslist)-1)]
        if jp_a == i:
            return "Neg"
        elif jp_a in self.locations[i].jp_list:
            return "Neg"
        else:
            return jp_a
   
   
    def jp_report(self):
        j = list(self.locations.keys())
        self.w = {}
        for i in j:
            self.w[i] = {}
            c.execute('''select origin from jumppoints where destination = ?''',(str(i),))
            r = c.fetchall()
            self.w[i]['Incoming'] = []
            for q in r:
                self.w[i]['Incoming'].append(q[0])
            c.execute('''select destination from jumppoints where origin = ?''',(str(i),))
            r = c.fetchall()
            self.w[i]['Outgoing'] = []
            for q in r:
                self.w[i]['Outgoing'].append(q[0])
        self.network = [0,] #always start with 0
        self.jps_checked = []
        for i in range(len(self.w)):
            self.check()
        print(len(self.locations), len(self.network))
        doubles = []
        for i in a.w:
            for y in a.w[i]['Incoming']:
                if y in a.w[i]['Outgoing']:
                    if not (i,y) or not (y,i) in doubles:
                        doubles.append((i,y))
        for i in doubles:
            c.execute('''delete from jumppoints where origin=? and destination=?''',i)
        conn.commit()
        conn.close()
         
    def check(self):
        for sid in self.network:
            if int(sid) not in self.jps_checked:
                self.jps_checked.append(sid)
                jps = []
                if int(sid) not in self.network:
                    self.network.append(int(sid))                    
                for i in self.w[sid]['Incoming']:
                    jps.append(i)
                for i in self.w[sid]['Outgoing']:
                    jps.append(i)
                for i in jps:
                    if i not in self.network:
                        self.network.append(i)
	
    def nameGen(self):
        first = ["","alpha","beta","gamma","delta","epsilon","zeta","eta","theta","iota","kappa","lambda","mu","nu","xi","omicron","pi","rho","sigma","tau","upsilon","phi","chi","psi","omega"]
		
        second = ["Andromedae","Antliae","Apodis","Aquarii","Aquilae","Arae","Arietis","Aurigae","Bootis","Caeli","Camelopardalis","Cancri","Canun Venaticorum","Canis Majoris","Canis Minoris","Capricorni","Carinae","Cassiopeiae","Centauri","Cephei","Ceti","Chamaleontis","Circini","Columbae","Comae Berenices","Coronae Australis","Coronae Borealis","Corvi","Crateris","Crucis","Cygni","Delphini","Doradus","Draconis","Equulei","Eridani","Fornacis","Geminorum","Gruis","Herculis","Horologii","Hydrae","Hydri","Indi","Lacertae","Leonis","Leonis Minoris","Leporis","Librae","Lupi","Lyncis","Lyrae","Mensae","Microscopii","Monocerotis","Muscae","Normae","Octantis","Ophiuchi","Orionis","Pavonis","Pegasi","Persei","Phoenicis","Pictoris","Piscium","Pisces Austrini","Puppis","Pyxidis","Reticuli","Sagittae","Sagittarii","Scorpii","Sculptoris","Scuti","Serpentis","Sextantis","Tauri","Telescopii","Trianguli","Trianguli Australis","Tucanae","Ursae Majoris","Ursae Minoris","Velorum","Virginis","Volantis","Vulpeculae","Norni","Minbari","Manticorae","Procyoni"]
        if (self.sndCnt == -1):
            self.sndCnt = []
            for i in range(0,len(second)):
                self.sndCnt.append(0)
        pick = randint(0,len(second)-1)
        self.sndCnt[pick] += 1
        return "{} {}".format(first[self.sndCnt[pick]],second[pick])

		         
class System:
    def __init__(self, name, sid, galaxy,jumppoints):
        self.stations = {}
        self.jp = jumppoints
        self.name = name
        number_of_stations = randint(1,3)
        for i in range(number_of_stations):
            col = list(types.keys())[randint(0,6)]
            station_name = self.name+" "+"I"*(1+i)
            self.stations[i] = Colony(col,station_name,sid,galaxy.count,)
            galaxy.count += 1
  
a = Galaxy()
a.jp_report()
