'''
Universe generator
this will define how the clusters (constlations) are put together and grouped on the map
this should be flexable enough to allow multiple ways and add the ability for systems to have custom names not always cluster names

Zaptan
2015/1/16
'''
from random import randint
from collections import namedtuple

class Universe:
    def __init__(self):
        self.possNames = ["Andromedae","Antliae","Apodis","Aquarii","Aquilae","Arae","Arietis","Aurigae","Bootis","Caeli","Camelopardalis","Cancri","Canun Venaticorum","Canis Majoris","Canis Minoris","Capricorni","Carinae","Cassiopeiae","Centauri","Cephei","Ceti","Chamaleontis","Circini","Columbae","Comae Berenices","Coronae Australis","Coronae Borealis","Corvi","Crateris","Crucis","Cygni","Delphini","Doradus","Draconis","Equulei","Eridani","Fornacis","Geminorum","Gruis","Herculis","Horologii","Hydrae","Hydri","Indi","Lacertae","Leonis","Leonis Minoris","Leporis","Librae","Lupi","Lyncis","Lyrae","Mensae","Microscopii","Monocerotis","Muscae","Normae","Octantis","Ophiuchi","Orionis","Pavonis","Pegasi","Persei","Phoenicis","Pictoris","Piscium","Pisces Austrini","Puppis","Pyxidis","Reticuli","Sagittae","Sagittarii","Scorpii","Sculptoris","Scuti","Serpentis","Sextantis","Tauri","Telescopii","Trianguli","Trianguli Australis","Tucanae","Ursae Majoris","Ursae Minoris","Velorum","Virginis","Volantis","Vulpeculae","Norni","Minbari","Manticorae","Procyoni"]
        self.possPrefix = ["alpha","beta","gamma","delta","epsilon","zeta","eta","theta","iota","kappa","lambda","mu","nu","xi","omicron","pi","rho","sigma","tau","upsilon","phi","chi","psi","omega"]
        self.numClusters = 0
        self.cluster = []
        self.grid = 0
    def sugSize(self,numSystems):
        return int(numSystems/12)+1
    def setClusters(self, num):
        self.numClusters = num
    def setGrid(self, num):
        self.grid = num
    def setupGrid(self):
        if (self.grid==0):
            self.grid = int(self.numClusters/4)+1
        grid=[]
        for y in range(1,self.grid):
            for x in range(1,self.grid):
                grid.append((x,y))
        return grid
    def genUniverse(self, numSystems):
        if (self.numClusters == 0):
            self.numClusters = self.sugSize(numSystems)
        grid=self.setupGrid()
        for c in range(0,self.numClusters):
            name = self.possNames.pop(randint(0,len(self.possNames)-1))
            pos = grid.pop(randint(0,len(grid)-1))
            trmp = (name,pos,[])
            self.cluster.append(trmp)
    def printUnv(self):
        for c in self.cluster:
            print(c)
        print(len(self.cluster))
