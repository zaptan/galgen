'''
this was a test, will be implemented in universe class
By Zaptan
'''

from PIL import Image, ImageDraw
from random import randint
import sqlite3
import os


db = "database.sql"
conn = sqlite3.connect(db)
c = conn.cursor()


im = Image.new
multiplyer = 6
size = (800*multiplyer,800*multiplyer)
off = multiplyer*3
im = Image.new('RGB', size)
draw = ImageDraw.Draw(im)


c.execute('''SELECT s.x, s.y, d.x, d.y
FROM systems as s, systems as d, jumppoints as j
WHERE s.sid = j.origin AND d.sid = j.destination
order by s.sid;''')
for row in c:
    draw.line((row[0]*multiplyer,row[1]*multiplyer,row[2]*multiplyer,row[3]*multiplyer),"#ffff00",3)
c.execute('''SELECT s.x, s.y, max(l.control), s.name
from locations as l, systems as s
where l.sid = s.sid
group by s.sid
order by s.sid;''')
for row in c:
	color = (int(255*(row[2]/10)),0,int(255*((10-row[2])/10)))
	draw.ellipse((row[0]*multiplyer-off,row[1]*multiplyer-off,row[0]*multiplyer+off,row[1]*multiplyer+off),color)

im.resize((800,800),Image.ANTIALIAS).save("map.png", 'PNG')
#im.save("map.png", 'PNG')
conn.close() 