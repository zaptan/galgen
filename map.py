'''
Map generator for the Universe
this was a test, will be implemented in universe class
By Zaptan
'''

from PIL import Image, ImageDraw, ImageFont
from random import randint
import sqlite3

def dups(seq):
    rset = set(seq)
    return list(rset)

grid = 20
db = "database.sql"
conn = sqlite3.connect(db)
c = conn.cursor()
c.execute('''select s.name from systems as s group by s.name;''')
names = []
for row in c:
    name = row[0].split(' ')
    names.append(" ".join(name[1:]))
conn.close
gridnames = []
names = sorted(dups(names))
possable = []
for y in range(1,grid):
    for x in range(1,grid):
        possable.append((x,y))

for n in names:
    gridnames.append([n,possable.pop(randint(0,len(possable)-1))])

im = Image.new
multi = 100
size = (grid*multi,grid*multi)
im = Image.new('RGB', size)
draw = ImageDraw.Draw(im)
font = ImageFont.truetype("arial.ttf", 10)

for p in gridnames:
    hw = draw.textsize(p[0],font=font)
    xy = p[1]
    pos = (int(xy[0]*multi - (hw[0]/2)), xy[1]*multi)
    draw.text(pos,p[0],font=font)

im.save("map.png", 'PNG')