#!/usr/bin/python

from PIL import Image, ImageDraw
import cStringIO, os
from random import randint, uniform, seed

from noise import pnoise2, snoise2
import cgi
import cgitb; cgitb.enable()

GET={}
args=os.getenv("QUERY_STRING").split('&')

for arg in args: 
    t=arg.split('=')
    if len(t)>1: k,v=arg.split('='); GET[k]=v


def randgradient():
    img = Image.new("RGB", (256,256), "#FFFFFF")
    iseed = 0
    iseed = GET.get('seed')
    if iseed!=0:
        seed(iseed)
    else:
        seed()


    roctaves = randint(1,80)
    rfreq = uniform(1.0,64.0) * roctaves
    goctaves = randint(1,80)
    gfreq = uniform(1.0,64.0) * goctaves
    boctaves = randint(1,80)
    bfreq = uniform(1.0,64.0) * boctaves

    npng = [0]*(256*256)
    i=0
    for y in range(256):
        for x in range(256):
            r=snoise2(x / rfreq, y / rfreq, roctaves) * 127.0 + 128.0
            g=snoise2(x / gfreq, y / gfreq, goctaves) * 127.0 + 128.0
            b=snoise2(x / bfreq, y / bfreq, boctaves) * 127.0 + 128.0
            npng[i] = (r,g,b)
            i += 1
        img.putdata(npng)

    f = cStringIO.StringIO()
    img.save(f, "PNG")

    print "Content-type: image/png\n"
    f.seek(0)
    print f.read()


if __name__ == "__main__":
    randgradient()